<?
    require "db.php";
    require "functions.php";
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Библиотека</title>
        <link rel="stylesheet" href="/css/main.css">
    </head>
    <body>
        <header>
            <ul>
                <li><a href="/">Главная</a></li>
                <?
                    if (isset($_SESSION["logged_user"])) {
                        if ($_SESSION["logged_user"]->role == "admin") {
                            echo "<li><a href=\"/users.php\">Пользователи</a></li>";
                        } else {
                            echo "<li><a href=\"/books.php\">Книги</a></li>";
                        }
                    }
                ?>
                <li>
                    <?
                        if (isset($_SESSION["logged_user"])) {
                            echo "<a href=\"/logout.php\">Выйти</a>";
                        } else {
                            echo "<a href=\"/login.php\">Войти</a>";
                        }
                    ?>
                </li>
            </ul>
        </header>
        <main>
            <?
                if (isset($_SESSION["logged_user"])) {
                    echo "<h2>Вы авторизованы как ".$_SESSION["logged_user"]->login."</h2>";
                    echo "<h2>Ваша роль: ".roleFull($_SESSION["logged_user"]->role)."</h2>";
                    echo "<br>";
                    echo "<h3 style=\"color: #136fb3\"><a href=\"/changepass.php\">Сменить пароль</a></h3>";
                } else {
                    echo "<h2>Вы не авторизованы</h2>";
                }
            ?>
        </main>
    </body>
</html>
