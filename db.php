<?
require "libs/rb.php";
require "config.php";

R::setup("pgsql:host=".DB_HOST.";dbname=".DB_NAME, DB_LOGIN, DB_PASSWORD);

$__admin__ = R::findOne("users", "login = ?", [ADM_LOGIN]);

if (!isset($__admin__)) {
    $user = R::dispense("users");
    $user->login = trim(ADM_LOGIN);
    $user->password = password_hash(ADM_PASSWORD, PASSWORD_DEFAULT);
    $user->role = "admin";
    R::store($user);
}

$__books__ = R::findAll("books");

foreach ($__books__ as $book) {
    if ($book->book_date < time()) {
        $book->booked = null;
        $book->book_date = null;
        R::store($book);
    }
}

if (EXAMPLE) {

    $__librarian__ = R::findOne("users", "login = ?", ["librarian"]);
    if (!isset($__librarian__)) {
        $user = R::dispense("users");
        $user->login = "librarian";
        $user->password = password_hash("1234567890", PASSWORD_DEFAULT);
        $user->role = "librarian";
        R::store($user);
    }

    $__client1__ = R::findOne("users", "login = ?", ["client1"]);
    if (!isset($__client1__)) {
        $user = R::dispense("users");
        $user->login = "client1";
        $user->password = password_hash("qwerty", PASSWORD_DEFAULT);
        $user->role = "client";
        R::store($user);
    }

    $__client2__ = R::findOne("users", "login = ?", ["client2"]);
    if (!isset($__client2__)) {
        $user = R::dispense("users");
        $user->login = "client2";
        $user->password = password_hash("WASD", PASSWORD_DEFAULT);
        $user->role = "client";
        R::store($user);
    }

    $__name1__ = "Звёздные Войны. Сверхдальний Перелет";
    $__book1__ = R::findOne("books", "name = ?", [$__name1__]);
    if (!isset($__book1__)) {
        $book = R::dispense("books");
        $book->name = $__name1__;
        $book->author = "Тимоти Зан";
        $book->genre = "фантастика";
        $book->publisher = "Азбука";
        $book->booked = null;
        $book->book_date = null;
        $book->given = null;
        R::store($book);
    }

    $__name2__ = "Смертельное путешествие";
    $__book2__ = R::findOne("books", "name = ?", [$__name2__]);
    if (!isset($__book2__)) {
        $book = R::dispense("books");
        $book->name = $__name2__;
        $book->author = "Кэти Райх";
        $book->genre = "роман";
        $book->publisher = "Азбука";
        $book->booked = null;
        $book->book_date = null;
        $book->given = null;
        R::store($book);
    }

    $__name3__ = "Дон Кихот";
    $__book3__ = R::findOne("books", "name = ?", [$__name3__]);
    if (!isset($__book3__)) {
        $book = R::dispense("books");
        $book->name = $__name3__;
        $book->author = "Мигель де Сервантес";
        $book->genre = "классика";
        $book->publisher = "Эксмо";
        $book->booked = null;
        $book->book_date = null;
        $book->given = null;
        R::store($book);
    }

    $__name4__ = "Отцы и дети";
    $__book4__ = R::findOne("books", "name = ?", [$__name4__]);
    if (!isset($__book4__)) {
        $book = R::dispense("books");
        $book->name = $__name4__;
        $book->author = "Иван Тургенев";
        $book->genre = "классика";
        $book->publisher = "Эксмо";
        $book->booked = null;
        $book->book_date = null;
        $book->given = null;
        R::store($book);
    }

}

session_start();
